(function ($) {
  ("use strict");
  // variables
  var layout = $(".layout"),
    header = $(".layout__header");
  // preloader
  preloader();
  function preloader() {
    layout.on("click", "a.nav__link", function (event) {
      layout.removeClass("layout_ready-load");
      event.preventDefault();
      var linkLocation = this.href;
      setTimeout(function () {
        window.location = linkLocation;
      }, 500);
    });
    setTimeout(function () {
      layout.addClass("layout_ready-load");
    }, 0);
  }

  // Slider
  if ($(".layout__slider").length) {
    $(".layout__slider").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 500,
      dots: false,
      autoplay: true,
      autoplayspeed: 500,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20" ><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
    });
  }

  // Banner
  if ($(".layout__banner").length) {
    $(".layout__banner").find(".slogan__list").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: false,
      speed: 500,
      fade: true,
      autoplay: true,
      autoplayspeed: 500,
    });
  }

  // Products Carousel
  if ($(".products__list_carousel").length) {
    $(".products__list_carousel").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 500,
      dots: false,
      autoplay: true,
      autoplayspeed: 500,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20" ><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
          },
        },
      ],
    });
  }
  // Products Carousel Tabs
  if ($(".layout__tabs .products__list_carousel").length) {
    $(window).on("load resize", function () {
      if ($(window).width() < 768) {
        if (
          $(".layout__tabs .products__list_carousel").hasClass(
            "slick-initialized"
          )
        ) {
          $(".layout__tabs .products__list_carousel").slick("unslick");
        }
      }
    });
  }

  // Tabs Primary
  if ($(".layout__tabs").length) {
    $(window).on("load resize", function () {
      if ($(window).width() > 768) {
        $(".layout__tabs")
          .find(".tabs__header")
          .find(".tabs__list_carousel")
          .slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            arrows: true,
            speed: 500,
            dots: false,
            prevArrow:
              '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20" ><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
            nextArrow:
              '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
          });
      } else if (
        $(".layout__tabs")
          .find(".tabs__header")
          .find(".tabs__list_carousel")
          .hasClass("slick-initialized")
      ) {
        $(".layout__tabs")
          .find(".tabs__header")
          .find(".tabs__list_carousel")
          .slick("unslick");
      }
    });
  }

  // Tabs Toggle
  if ($(".tabs__toggle").length) {
    let tabsActive = "active";
    $(".layout__section").on("click", ".tabs__toggle", function () {
      let name = $(this).data("name");
      $(this).addClass(tabsActive).siblings().removeClass(tabsActive);

      $(".tabs__content[data-name=" + name + "]")
        .slideToggle(0)
        .siblings()
        .slideUp(0);
    });
  }

  // Clients Carousel
  if ($(".clients__list_carousel").length) {
    $(".clients__list_carousel").slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 500,
      dots: false,
      autoplay: true,
      autoplaySpeed: 1000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20" ><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 580,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }

  // Article__slider
  if ($(".article__slider").length) {
    $(".article__slider").find(".slider__list").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 500,
      dots: false,
      fade: true,
      autoplay: true,
      autoplayspeed: 500,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20" ><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
    });
  }

  // Case slider
  if ($(".case__slider").length) {
    $(".case__slider").find(".slider__list").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 500,
      dots: false,
      fade: true,
      autoplay: true,
      autoplayspeed: 500,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20" ><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
    });
  }

  // Manufactures Carousel
  if ($(".manufactures__gallery").length) {
    let gallery = $(".manufactures__gallery").find(".gallery__list");
    gallery.each(function () {
      $(this).slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        speed: 500,
        dots: false,
        infinite: false,
        autoplay: true,
        autoplayspeed: 500,
        prevArrow:
          '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
        nextArrow:
          '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
        responsive: [
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
        ],
      });
    });
  }

  // Product__slider
  if ($(".product__slider").length) {
    $(".product__slider").find(".slider__list_primary").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      draggable: false,
      swipe: false,
      swipeToSlide: false,
      touchMove: false,
      draggable: false,
      asNavFor: ".slider__list_secondary",
    });
    $(".product__slider").find(".slider__list_secondary").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: ".slider__list_primary",
      focusOnSelect: true,
      draggable: false,
      swipe: false,
      swipeToSlide: false,
      touchMove: false,
      draggable: false,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20" ><path d="M7.16846 17L7.16846 11.4L19.957 11.4L20 8.586L7.16846 8.586L7.16846 3L-3.0598e-07 10L7.16846 17Z" /></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><g clip-path="url(#clip0_60_1961)"><path d="M12.8325 3.00024L12.8325 8.60024L0.0439889 8.60024L0.000976195 11.4142L12.8325 11.4142L12.8325 17.0002L20.001 10.0002L12.8325 3.00024Z" /></g><defs><clipPath id="clip0_60_1961"><rect width="20" height="20" transform="translate(20.001 0.000244141) rotate(90)"/></clipPath></defs></svg></div>',
    });
  }

  // Menu
  navInit();
  function navInit() {
    header.find(".header__burger").on("click", function () {
      $(this).closest(header).toggleClass("layout__header_menu-active");
      $(this).closest(header).find(".header__nav").slideToggle(200);
      $(this).closest(header).find(".header__contact").slideToggle(200);
    });
  }

  // Submenu
  dropDownInit();
  function dropDownInit() {
    header.find(".nav__item").on("click", function () {
      if ($(window).width() < 768) {
        $(this).toggleClass("nav__item_active");
        $(this).find(".nav__sublist").slideToggle(200);
      }
    });
  }

  // Catalog
  catalogInit();
  function catalogInit() {
    header
      .find(".header__catalog")
      .on("click", ".catalog__preview", function () {
        $(this)
          .closest(".header__catalog")
          .toggleClass("header__catalog_active");
        $(this).siblings(".catalog__dropdown").slideToggle(200);
      });
  }

  // Search
  searchInit();
  function searchInit() {
    header.find(".header__search").on("click", ".search__header", function () {
      $(this).closest(".header__search").toggleClass("header__search_active");
      $(this).siblings(".search__main").slideToggle(200);
    });
  }

  /// Scroll functions
  $(window).on("load resize scroll", function () {
    let h = $(window).height();
    scrollHeader(h);
    scrollSection(h);
    scrollImage(h);
  });
  function scrollHeader(h) {
    if ($(window).scrollTop() >= 180) {
      header.addClass("layout__header_move");
    } else {
      header.removeClass("layout__header_move");
    }
    if ($(window).scrollTop() >= 181) {
      document.addEventListener("wheel", (e) => {
        if (e.wheelDeltaY > 0 && $(window).scrollTop() >= h / 2) {
          header.addClass("layout__header_animation");
        } else {
          header.removeClass("layout__header_animation");
        }
      });
    } else {
      header.removeClass("layout__header_animation");
    }
  }
  function scrollSection(h) {
    let section = $(".section");
    section.each(function () {
      if ($(window).scrollTop() + h >= $(this).offset().top) {
        $(this).addClass("section_animation");
      }
    });
  }
  function scrollImage(h) {
    // Image initialization
    let img = $("img");
    img.each(function () {
      if (
        $(window).scrollTop() + 1.5 * h >= $(this).offset().top &&
        this.getAttribute("data-src") &&
        this.src !== this.getAttribute("data-src")
      ) {
        this.src = this.getAttribute("data-src");
      }
    });
  }

  // Tabs init
  if ($(".tabs").length) {
    tabsInit();
  }

  function tabsInit() {
    let tabsActive = "tabs__item_active";
    $(".tabs__header").on("click", ".tabs__item", function () {
      let name = $(this).data("name");
      $(this).addClass(tabsActive).siblings().removeClass(tabsActive);

      $(".tabs__main")
        .find(".tabs__item[data-name=" + name + "]")
        .addClass(tabsActive)
        .siblings()
        .removeClass(tabsActive);
    });

    let scrollContainer = document.querySelector(".tabs__list");
    scrollContainer.addEventListener("wheel", (evt) => {
      evt.preventDefault();
      if (
        (evt.deltaY > 0) &
        (Math.ceil(scrollContainer.scrollLeft + scrollContainer.clientWidth) >=
          Math.ceil(scrollContainer.scrollWidth))
      ) {
      } else if (
        evt.deltaY <= 0 &&
        Math.ceil(scrollContainer.scrollLeft) === 0
      ) {
      }

      scrollContainer.scrollLeft += evt.deltaY;
    });
  }

  // Accordion init
  if ($(".accordion").length) {
    accordionInit();
  }

  function accordionInit() {
    $(".accordion").on("click", ".accordion__header", function () {
      $(this).closest(".accordion__item").toggleClass("accordion__item_active");
      $(this)
        .closest(".accordion__item")
        .find(".accordion__dropdown")
        .slideToggle(200);
    });
  }

  // Filter init
  if ($(".catalog__filters").length) {
    filterInit();
  }

  function filterInit() {
    $(".catalog__filters").on("click", ".filters__header", function () {
      $(this)
        .closest(".catalog__filters")
        .toggleClass("catalog__filters_active");
      $(this).siblings(".filters__main").slideToggle(200);
    });
  }

  // Validation & customize form
  if ($("form").length) {
    formValidation();
    function formValidation() {
      let form = $("form");
      form.submit(function () {
        if ($(this).valid()) {
          return true;
        } else {
          return false;
        }
      });
      form.validate({
        rules: {
          name: {
            required: true,
            name: true,
          },
          email: {
            required: true,
            email: true,
          },
          phone: {
            required: true,
          },
        },
      });
    }
    // Mask form
    $(".phone_mask").mask("8(999) 999-99-99");

    /* Calculator */
    if ($(".calc__range").length) {
      calcRange();
    }
    function calcRange() {
      $(".calc__range_primary").slider({
        range: "min",
        min: 300000,
        max: 35000000,
        slide: function (event, ui) {
          $(this)
            .closest(".calc__group")
            .find(".price__value")
            .text(new Intl.NumberFormat("ru-RU").format(ui.value));
        },
      });
      $(".calc__range_secondary").slider({
        range: "min",
        min: 300000,
        max: 35000000,
        slide: function (event, ui) {
          $(this)
            .closest(".calc__group")
            .find(".price__value")
            .text(new Intl.NumberFormat("ru-RU").format(ui.value));
        },
      });
      $(".calc__range_tertiary").slider({
        range: "min",
        min: 0,
        max: 12,
        slide: function (event, ui) {
          $(this)
            .closest(".calc__group")
            .find(".price__value")
            .text(new Intl.NumberFormat("ru-RU").format(ui.value));
        },
      });
    }

    if ($(".range__slider").length) {
      filterRange();
    }
    function filterRange() {
      let min = 0,
        max = 2000,
        step = 100;
      $(".range__slider").slider({
        range: true,
        min: min,
        max: max,
        step: step,
        change: function (event) {
          var parent = event.target.closest(".filter__range");
          if (parent != null) {
            var input = parent.querySelector("input");
            if (input != null) {
              input.dispatchEvent(new Event("input"));
            }
          }
        },
        slide: function (event, ui) {
          console.log(ui.values[0]);

          $(".filter__range")
            .find(".range__header")
            .find(".range__item_min")
            .find(".range__val")
            .val(ui.values[0]);
          $(".filter__range")
            .find(".range__header")
            .find(".range__item_max")
            .find(".range__val")
            .val(ui.values[1]);
        },

        create: function (event, ui) {
          $(this).slider(
            "values",
            0,
            $(".filter__range")
              .find(".range__header")
              .find(".range__item_min")
              .find(".range__val")
              .val()
          );
          $(this).slider(
            "values",
            1,
            $(".filter__range")
              .find(".range__header")
              .find(".range__item_max")
              .find(".range__val")
              .val()
          );
          if (min === max) {
            $(this).addClass("disabled");
          }
        },
      });
      $(".filter__range")
        .find(".range__header")
        .find(".range__item_min")
        .find(".range__val")
        .val($(".range__slider").slider("values", 0));
      $(".filter__range")
        .find(".range__header")
        .find(".range__item_min")
        .find(".range__val")
        .val($(".range__slider").slider("values", 1));

      $(".filter__range")
        .find(".range__item_min")
        .find(".range__val")
        .change(function () {
          var value = $(this).val();

          if ($(this).val() != "") {
            if (parseInt($(this).val()) < min) {
              $(this).val(min);
            }
            if (parseInt($(this).val()) > parseInt(max)) {
              $(this).val(max);
            }
          }
          console.log(parseInt($(this).val()));
          $(this)
            .closest(".filter__range")
            .find(".range__slider")
            .slider("values", 0, value);
        });
      $(".filter__range")
        .find(".range__item_max")
        .find(".range__val")
        .change(function () {
          var value = $(this).val();
          if ($(this).val() != "") {
            if (parseInt($(this).val()) < min) {
              $(this).val(min);
            }
            if (parseInt($(this).val()) > parseInt(max)) {
              $(this).val(max);
            }
          }
          $(this)
            .closest(".filter__range")
            .find(".range__slider")
            .slider("values", 1, value);
        });
    }
  }

  // MODAL INIT
  modalInit();
  function modalInit() {
    let modalName;
    // modal show
    $(document).on("click", ".modal-init", function () {
      layout
        .addClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
      modalName = $(this).data("modalname");
      layout.find("." + modalName + "").addClass("modal__layout_active");
    });
    // modal hide
    $(document).mouseup(function (e) {
      if ($(".modal__layout_active").length) {
        var div = $(".modal__layout");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
          modalHide();
        }
      }
    });
    // modal hide
    $(document).on("click", ".modal__action", function () {
      modalHide();
    });
    // modal hide
    $(window).keydown(function (e) {
      if (e.key === "Escape") {
        modalHide();
      }
    });

    function modalHide() {
      layout
        .removeClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
    }
  }

  if ($(".layout__contact").length) {
    contactInit();
    contactFunction();
  }

  function contactInit() {
    // ymaps.ready(init);
    // var customMap;
    // function init() {
    //   customMap = new ymaps.Map("contact__iframe", {
    //     center: [59.88419, 30.389302],
    //     behaviors: ["default", "scrollZoom"],
    //     zoom: 17,
    //     controls: [],
    //   });
    //   customPlacemark0 = new ymaps.Placemark(
    //     [59.88419, 30.389302],
    //     {},
    //     {
    //       iconLayout: "default#image",
    //       iconImageHref: "img/pin.png",
    //       iconImageSize: [120, 120],
    //       iconImageOffset: [-18, -36],
    //     }
    //   );
    //   customMap.geoObjects.add(customPlacemark0);
    // }
  }
  function contactFunction() {
    ymaps.ready(function () {
      var $map = $("#contact__iframe"),
        COORDS = $map.attr("data-coords").split(",").map(parseFloat),
        OFFSET_Y = 0.004,
        OFFSET_X = 0.012,
        CENTER_TABLET_HORIZONTAL = [COORDS[0], COORDS[1] - OFFSET_X],
        CENTER_TABLET_VERTICAL = [COORDS[0] + OFFSET_Y, COORDS[1]];
      var map = new ymaps.Map($map[0], {
        center: CENTER_TABLET_HORIZONTAL,
        zoom: 15,
        controls: ["zoomControl", "fullscreenControl"],
      });

      var marker = new ymaps.Placemark(
        COORDS,
        {},
        {
          iconLayout: "default#image",
          iconImageHref: "img/marker.svg",
          iconImageSize: [138, 180],
          iconImageOffset: [-66, -162],
        }
      );

      map.geoObjects.add(marker);
      map.behaviors.disable("scrollZoom");
      toggleDrag();
      toggleCenter();
      $window.on("resize", toggleDrag).on("resize", toggleCenter);

      function toggleDrag() {
        var isDrag = map.behaviors.isEnabled("drag");
        if (isMobile && isDrag) {
          map.behaviors.disable("drag");
        }
        if (!isMobile && !isDrag) {
          map.behaviors.enable("drag");
        }
      }

      function toggleCenter() {
        var CENTER = map.getCenter().map(function (el) {
          return +el.toFixed(5);
        });

        if (
          !breakpoint.isTabletVertical &&
          CENTER[0] !== CENTER_TABLET_HORIZONTAL[0]
        ) {
          map.setCenter(CENTER_TABLET_HORIZONTAL);
        }
        if (
          breakpoint.isTabletVertical &&
          CENTER[0] !== CENTER_TABLET_VERTICAL[0]
        ) {
          map.setCenter(CENTER_TABLET_VERTICAL);
        }
      }
    });
  }
})(jQuery);
