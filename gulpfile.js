var gulp = require("gulp"),
  injectPartials = require("gulp-inject-partials"),
  browserSync = require("browser-sync"),
  autoprefixer = require("gulp-autoprefixer"),
  concat = require("gulp-concat"),
  minify = require("gulp-minify"),
  sass = require("gulp-sass")(require("sass"));

gulp.task(
  "watch",
  gulp.series(function () {
    browserSync({
      server: {
        baseDir: "./build",
        index: "index.html",
        directory: true,
      },
      watchTask: true,
    });
    gulp
      .src(["node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css"])
      .pipe(concat("libs.min.css"))
      .pipe(gulp.dest("build/css"));
    gulp
      .src([
        "node_modules/jquery-validation/dist/jquery.validate.js",
        "node_modules/slick-carousel/slick/slick.min.js",
        "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js",
        "node_modules/jquery.maskedinput/src/jquery.maskedinput.js",
      ])
      .pipe(concat("libs.js"))
      .pipe(
        minify({
          ext: {
            min: ".min.js",
          },
          ignoreFiles: ["-min.js"],
        })
      )
      .pipe(gulp.dest("build/js"));
    var css = gulp.watch("./src/scss/**/*.sass");
    css.on("change", function () {
      gulp
        .src("src/scss/main.sass")
        .pipe(sass())
        .pipe(autoprefixer(["last 2 versions"], { cascade: true }))
        .pipe(gulp.dest("build/css"));

      browserSync.reload();
    });

    var html = gulp.watch("./src/templates/**/*.html");
    html.on("change", function () {
      gulp
        .src("./src/templates/*.html")
        .pipe(injectPartials())
        .pipe(gulp.dest("./build"));

      browserSync.reload();
    });

    var js = gulp.watch("./src/js/*.js");
    js.on("change", function () {
      gulp.src(["./src/js/main.js"]).pipe(gulp.dest("build/js"));
      browserSync.reload();
    });
  })
);
